package ru.tsc.karbainova.tm.command;

import ru.tsc.karbainova.tm.api.service.ServiceLocator;
import ru.tsc.karbainova.tm.enumerated.Role;

public abstract class AbstractCommand {
    protected ServiceLocator serviceLocator;

    public AbstractCommand() {
    }

    public void setServiceLocator(ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public Role[] roles() {
        return null;
    }

    public abstract String name();

    public abstract String arg();

    public abstract String description();

    public abstract void execute();

    @Override
    public String toString() {
        String result = "";
        String name = name();
        String arg = arg();
        String description = description();

        if (name != null && !name.isEmpty()) result += name + " ";
        if (arg != null && !arg.isEmpty()) result += " [" + arg + "] ";
        if (description != null && !description.isEmpty()) result += description;
        return result;
    }

}
