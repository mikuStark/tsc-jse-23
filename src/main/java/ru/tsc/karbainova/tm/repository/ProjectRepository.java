package ru.tsc.karbainova.tm.repository;

import lombok.NonNull;
import ru.tsc.karbainova.tm.api.repository.IProjectRepository;
import ru.tsc.karbainova.tm.model.Project;
import ru.tsc.karbainova.tm.model.User;


import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;

public class ProjectRepository extends AbstractOwnerRepository<Project> implements IProjectRepository {

    @NonNull
    private final List<Project> projects = new ArrayList<>();

    @Override
    public boolean existsById(String userId, String id) {
        return findById(userId, id) != null;
    }

    @Override
    public Project findByIndex(@NonNull String userId, @NonNull int index) {
        final List<Project> entities = findAll(userId);
        return entities.get(index);
    }

    @NonNull
    public final Predicate<Project> predicateByName(@NonNull final String name) {
        return s -> name.equals(s.getName());
    }

    @Override
    public Project findByName(@NonNull String userId, @NonNull String name) {
        final List<Project> entities = findAll(userId);
        return entities.stream().filter(predicateByName(name)).findFirst().orElse(null);
    }

    @Override
    public Project removeByName(@NonNull String userId, @NonNull String name) {
        final Project project = findByName(userId, name);
        if (project == null) return null;
        entities.remove(project);
        return project;
    }

    @Override
    public Project removeByIndex(String userId, int index) {
        final Project project = findByIndex(userId, index);
        if (project == null) return null;
        remove(userId, project);
        return project;
    }
}
