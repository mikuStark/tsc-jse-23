package ru.tsc.karbainova.tm.util;

import lombok.NonNull;
import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.Nullable;

@UtilityClass
public class HashUtil {
    @NonNull
    String SECRET = "1231231231";

    Integer ITERATION = 12345;

    @NonNull
    public static String salt(@NonNull final String value) {
        String result = value;
        for (int i = 0; i < ITERATION; i++) {
            result = md5(SECRET + result + SECRET);
        }
        return result;
    }

    @Nullable
    public static String md5(@NonNull final String value) {
        try {
            @NonNull java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            final byte[] array = md.digest(value.getBytes());
            @NonNull final StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; i++) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }
}
