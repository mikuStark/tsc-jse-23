package ru.tsc.karbainova.tm.model;

import lombok.*;
import org.jetbrains.annotations.Nullable;
import ru.tsc.karbainova.tm.api.entity.IWBS;
import ru.tsc.karbainova.tm.enumerated.Status;

import java.util.Date;

@Getter
@Setter


public class Task extends AbstractOwnerEntity implements IWBS {

    public Task() {
    }

    public Task( String name) {
        this.name = name;
    }

    public Task( String name, String description) {
        this.name = name;
        this.description = description;
    }

    private String name;


    private String description;

    private Status status = Status.NOT_STARTED;

    private String projectId = null;

    private Date startDate;

    private Date finishDate;

    private Date created = new Date();


    @Override
    public String toString() {
        return " " + name + " ";
    }

}
