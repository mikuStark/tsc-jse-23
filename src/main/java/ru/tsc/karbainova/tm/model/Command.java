package ru.tsc.karbainova.tm.model;

import lombok.*;
import org.jetbrains.annotations.Nullable;

@Getter

@AllArgsConstructor

public class Command {


    private final String name;

    private final String argument;

    private final String description;


    @Override
    public String toString() {
        String result = "";
        if (name != null && !name.isEmpty()) result += name + " ";
        if (argument != null && !argument.isEmpty()) result += "(" + argument + ") ";
        if (description != null && !description.isEmpty()) result += description + " ";
        return result;
    }
}
